package pcd1718.ass3.oneToMany

case class Cell(x: Int, y: Int){
  lazy val neighbours: Iterable[Cell] =
    for(nx <- x-1 to x+1;
        ny <- y-1 to y+1;
        if nx>=0 && ny>=0 && (nx!=x || ny!=y) ) yield Cell(nx,ny)
}
case class Subgrid(fromx: Int, fromy: Int, tox: Int, toy: Int){
  def deadData: SubgridData =
    SubgridData(
      this,
      (for(x <- fromx to tox; y <- fromy to toy) yield Cell(x,y) -> Dead) toMap)
}
case class SubgridData(subgrid: Subgrid, data: Map[Cell,State]){
  def nextState(outerStates: Map[Cell,State]): SubgridData = SubgridData(subgrid, data.map { case (cell,state) =>
      val alives = cell.neighbours.foldLeft(0)((alives,nbr) => alives + data.get(nbr).orElse(outerStates.get(nbr)).count(_==Alive))
      if(state==Alive || alives>0) println(s"Alive cell $cell -> ${cell.neighbours} of which $alives alives")
      cell -> State(state, alives)
  })

  def flip(c: Cell) = SubgridData(subgrid, data + (c -> data(c).flip))
}

case class Setup(cols: Int, rows: Int, ratio: Int){
  def computeSubgrids(): Iterable[Subgrid] = {
    val gridSide = Math.sqrt(this.ratio).toInt
    val krows = gridSide
    val kcols = gridSide

    var subgrids = List()
    for(i <- 0 until this.cols by kcols;
        j <- 0 until this.rows by krows)
      yield Subgrid(i, j, Math.min(cols-1, i+kcols-1), Math.min(rows-1, j+krows-1))
  }

  def gridByCell(c: Cell): Subgrid = {
    val Cell(x,y) = c
    val gridSide = Math.sqrt(ratio).toInt

    val offsetX = x % gridSide
    val offsetY = y % gridSide
    Subgrid(
      fromx = x-offsetX,
      fromy = y-offsetY,
      tox = Math.min(cols-1, x-offsetX+gridSide-1),
      toy = Math.min(rows-1, y-offsetY+gridSide-1))
  }

  /**
    * [][]|[][]|[][]|[][]
    * [][]|[][]|[][]|[][]   Grids 0,1,2,3
    * ----+----+----+----
    * [][]|[][]|[][]|[][]
    * [][]|[][]|[]XX|[][]   Grids 4,5,6,7
    * ----+----+----+----
    * [][]|[][]|[][]|[][]
    * [][]|[][]|[][]|[][]   Grids 8,9,10,11
    * The output should be: 6
    */
  def subgridNumByCell(c: Cell): Int = {
    val Cell(x,y) = c
    val gridSide = Math.sqrt(ratio).toInt

    val gridsPerRow = cols / gridSide
    val gridX: Int = x / gridSide
    val gridY: Int = y / gridSide

    gridY*gridsPerRow + gridX
  }
}

trait State {
  def flip = this match {
    case Alive => Dead
    case Dead => Alive
  }
}
object State {
  def apply(state: State, alives: Int) =
    if(state==Alive && (alives<2 || alives>3)) Dead
    else if(state==Dead && alives!=3) Dead
    else Alive
}
case object Alive extends State
case object Dead extends State