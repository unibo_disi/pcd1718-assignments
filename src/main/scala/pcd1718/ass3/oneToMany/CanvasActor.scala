package pcd1718.ass3.oneToMany

import javafx.application.Platform
import javafx.scene.canvas.Canvas
import javafx.scene.paint.Color

import akka.actor.{Actor, ActorLogging, ActorRef}

class CanvasActor(val canvas: Canvas) extends Actor with ActorLogging {
  val g = canvas.getGraphicsContext2D

  var nrows = 0
  var ncols = 0
  var side = 0.0
  var states: Map[Cell,State] = Map()

  var gol: Option[ActorRef] = None

  def showState(x: Int, y: Int, s: State): Unit = {
    if (s == Alive) {
      g.setFill(Color.YELLOW)
      g.fillRect(x * side + 0.5, y * side + 0.5, side - 1, side - 1)
    }
    else {
      g.setFill(Color.BLACK)
      g.fillRect(x * side + 0.5, y * side + 0.5, side - 1, side - 1)
    }
  }

  override def receive: Receive = {
    case Click(clickX,clickY) =>
      log.debug(s"Got click $clickX $clickY")
      val (x,y) = lookup(clickX, clickY)
      gol.foreach(_ ! FlipState(Cell(x,y)))
    case SetGrid(s, map) =>
      gol = Some(sender)
      states = map
      buildGrid(s)
    case SetState(c,s) =>
      states += c -> s
      Platform.runLater(() => showState(c.x,c.y,s))
    case DrawSystem(map) => {
      val flips = map.filter(c => states(c._1)!=c._2)
      log.debug(s"Map size: ${map.size}; flips: ${flips.size}")
      states = map
      Platform.runLater(() => flips.foreach{ case (Cell(x,y), state) => showState(x,y,state) })
    }
  }

  def buildGrid(s: Setup): Unit = {
    nrows = s.rows
    ncols = s.cols
    side = Math.min(canvas.getWidth / ncols, canvas.getHeight / nrows)

    log.info(s"Building grid ${nrows}x${ncols}, each cell of side $side.")

    g.setFill(Color.BLACK)
    g.setStroke(Color.ALICEBLUE)
    g.setLineWidth(0.2)
    g.fillRect(0, 0, canvas.getWidth, canvas.getHeight)
    g.setFill(Color.YELLOW)

    for (row <- 0 until nrows;
         col <- 0 until ncols) {
      g.strokeRect(row * side, col * side, side, side)
      showState(col, row, states(Cell(col,row)))
    }
  }

  def lookup(x: Double, y: Double): (Int, Int) = {
    (Math.floor(x/side).toInt, Math.floor(y/side).toInt)
  }
}
