package pcd1718.ass3.oneToMany

import akka.actor.{Actor, ActorLogging, ActorRef}
import pcd1718.ass3._

class SubgridActor(val subgrid: Subgrid) extends Actor with ActorLogging {
  private var nbrs: Vector[ActorRef] = Vector.empty
  private var state: SubgridData = subgrid.deadData
  private var rounds: Map[Round,RoundData] = Map()

  override def receive: Receive = computationBehaviour orElse configurationBehavior

  val configurationBehavior: Receive = {
    case Neighbour(ref) =>
      nbrs = nbrs :+ ref
    case FlipState(cell) =>
      sender ! { state = state.flip(cell); state }
  }

  val computationBehaviour: Receive = {
    case r @ Round(k) =>
      log.info(s"$subgrid > Initiating round $k by sending my state to $nbrs")
      nbrs.map(_ ! MyState(r, state))
    case ms@MyState(r,s) =>
      gotOne(r,s)
  }

  def gotOne(r: Round, s: SubgridData) = {
    if(!rounds.contains(r)){
      nbrs.map(_ ! MyState(r, state))
      rounds += r -> RoundData(0,Map())
    }

    rounds += r -> rounds(r).merge(s.data)

    if(rounds(r).got==nbrs.size){
      state = state.nextState(rounds(r).map)
      context.parent ! MyState(r, state)
    }
  }

  private case class RoundData(got: Int, map: Map[Cell,State]){
    def merge(toMerge: Map[Cell,State]) = RoundData(got+1, map ++ toMerge)
  }
}
