package pcd1718.ass3.oneToMany

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.duration.FiniteDuration
import scala.util.Success

class GOLActor(canvas: ActorRef) extends Actor with ActorLogging {
  override def receive = {
    case s: Setup =>
      setup(s)
      canvas ! SetGrid(s, cellMap)
      context.become(workingBehaviour)
  }

  var setupConfig: Setup = _
  var children: Map[Subgrid, ActorRef] = Map.empty
  var states: Map[Subgrid, SubgridData] = Map.empty
  var got = 0
  var ticks = 0
  var started = false

  def cellMap = states.flatMap(_._2.data)

  // For Ask pattern
  implicit val timeout = new Timeout(FiniteDuration(2,TimeUnit.SECONDS))
  implicit val ec = context.system.dispatcher

  val workingBehaviour: Receive = {
    case Start =>
      started = true
      self ! Tick
    case Stop =>
      started = false
    case Tick =>
      ticks = ticks + 1
      log.debug(s"Tick $ticks")
      children.valuesIterator.collectFirst { case ref => ref ! Round(ticks) }
      got = 0
      context.become(roundBehaviour)
    case s: Setup =>
      setup(s)
      canvas ! SetGrid(s, cellMap)
    case FlipState(cell) =>
      flipState(cell)
  }

  val roundBehaviour: Receive = {
    case Stop =>
      started = false
      context.become(workingBehaviour)
    case MyState(_, data) =>
      states += data.subgrid -> data
      got = got + 1
      if(got == children.size) {
        log.info(s"Round done!")
        canvas ! DrawSystem(cellMap)
        if(started) self ! Tick
        context.become(workingBehaviour)
      }
  }

  def setup(s: Setup) = {
    clear()
    log.info(s"Setup matrix ${s.rows}x${s.cols}")

    this.setupConfig = s
    val subgrids = s.computeSubgrids()
    children = Map.empty

    for(g <- subgrids) {
      val child = context.actorOf(Props(classOf[SubgridActor], g))
      children += g -> child
      states += g -> g.deadData
      log.info(""+g)
    }

    for(c <- children;
        x <- subgrids) {
      val g = c._1
      if (adj(x.fromx, g.tox) || adj(x.tox, g.fromx) || adj(x.fromy, g.toy) || adj(x.toy, g.fromy))
        c._2 ! Neighbour(children(x))
    }
  }

  def adj(a: Int, b: Int) =
    Math.abs(a-b) == 1

  def flipState(cell: Cell) = {
    val g = this.setupConfig.gridByCell(cell)
    (children(g) ? FlipState(cell)).onComplete {
      case Success(ns: SubgridData) =>
        states += g -> states(g).flip(cell)
        canvas ! SetState(cell, ns.data(cell))
    }
  }

  def clear() = {
    children.valuesIterator.foreach(context.stop(_))
  }

  private def childByCell(c: Cell): ActorRef =
    children(setupConfig.gridByCell(c))
}