package pcd1718.ass3.utils

import akka.actor.{Actor, ActorRef}

case class AddObserver(ref: ActorRef)
case class RemoveObserver(ref: ActorRef)

trait ObservableBehaviour { self: Actor =>
  var observers: Set[ActorRef] = Set()

  def observableBehaviour: Receive = {
    case AddObserver(ref) =>
      observers += ref
    case RemoveObserver(ref) =>
      observers -= ref
  }
}