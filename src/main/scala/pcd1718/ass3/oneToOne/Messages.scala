package pcd1718.ass3.oneToOne

import akka.actor.ActorRef

case class Setup(cols: Int, rows: Int, ratio: Int)
case object Tick
case class Round(num: Int)
case object Start
case object Stop
case object Pause
case object Resume
case object Clear

case class Neighbour(ref: ActorRef)
case class MyState(round: Round, cell: Cell, state: State)
case object YourState
case class FlipState(cell: Cell)
object RoundDone

case class Click(x: Double, y: Double)
case class SetGrid(setup: Setup, map: Map[Cell,State])
case class SetState(cell: Cell, state: State)
case class DrawSystem(map: Map[Cell,State])