package pcd1718.ass3.oneToOne

trait State
case object Alive extends State
case object Dead extends State
case class Cell(x: Int, y: Int)