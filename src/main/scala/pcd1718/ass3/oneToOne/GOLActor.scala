package pcd1718.ass3.oneToOne

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import pcd1718.ass3._

import scala.concurrent.duration.FiniteDuration
import scala.util.Success

class GOLActor(canvas: ActorRef) extends Actor with ActorLogging {
  override def receive = {
    case s: Setup =>
      setup(s)
      canvas ! SetGrid(s, states)
      context.become(workingBehaviour)
  }

  var cells: Array[Array[ActorRef]] = Array.empty
  var states: Map[Cell,State] = Map()
  var totCells = 0
  var got = 0
  var ticks = 0
  var started = false

  // For Ask pattern
  implicit val timeout = new Timeout(FiniteDuration(2,TimeUnit.SECONDS))
  implicit val ec = context.system.dispatcher

  val workingBehaviour: Receive = {
    case Start =>
      started = true
      self ! Tick
    case Stop =>
      started = false
    case Tick =>
      ticks = ticks + 1
      log.debug(s"Tick $ticks")
      cells(0)(0) ! Round(ticks)
      got = 0
      context.become(roundBehaviour)
    case s: Setup =>
      setup(s)
      canvas ! SetGrid(s, states)
    case FlipState(cell) =>
      flipState(cell)
  }

  val roundBehaviour: Receive = {
    case Stop =>
      started = false
      context.become(workingBehaviour)
    case MyState(_, cell,state) =>
      states += cell -> state
      got = got + 1
      if(got == totCells) {
        log.info(s"Round done!")
        canvas ! DrawSystem(states)
        if(started) self ! Tick
        context.become(workingBehaviour)
      }
  }

  def setup(s: Setup) = {
    clear()
    log.info(s"Setup matrix ${s.rows}x${s.cols}")

    totCells = s.rows*s.cols
    cells = Array.ofDim[ActorRef](s.rows,s.cols)
    states = Map()
    for(i <- 0 until s.rows;
        j <- 0 until s.cols) {
      val cell = Cell(i,j)
      cells(i)(j) = context.actorOf(Props(classOf[CellActor], cell))
      val ns = (s.rows-i-1, j) match {
        case (0, 2) => Alive
        case (1, 0) => Alive
        case (1, 2) => Alive
        case (2, 1) => Alive
        case (2, 2) => Alive
        case _ => Dead
      }
      states += cell -> ns
      if(ns == Alive) flipState(cell)
    }

    for(i <- 0 until s.rows;
        j <- 0 until s.cols) {
        for(ni <- i-1 to i+1 if ni>=0 && ni<s.rows;
            nj <- j-1 to j+1 if (nj>=0 && nj<s.cols) && !(ni==i && nj==j)) {
          cells(i)(j) ! Neighbour(cells(ni)(nj))
        }
    }
  }

  def flipState(cell: Cell) =
    (cells(cell.x)(cell.y) ? FlipState(cell)).onComplete{
      case Success(ns: State) =>
        states += cell -> ns
        canvas ! SetState(cell,ns)
    }

  def clear() = {
    cells.foreach(_.foreach(context.stop(_)))
  }
}
