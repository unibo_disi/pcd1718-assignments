package pcd1718.ass3.oneToOne

import akka.actor.{Actor, ActorLogging, ActorRef}
import pcd1718.ass3._

class CellActor(val cell: Cell) extends Actor with ActorLogging {
  private var nbrs: Vector[ActorRef] = Vector.empty
  private var state: State = Dead
  private var rounds: Map[Round,RoundData] = Map()

  override def receive: Receive = computationBehaviour orElse configurationBehavior

  val configurationBehavior: Receive = {
    case Neighbour(ref) =>
      nbrs = nbrs :+ ref
    case FlipState(`cell`) =>
      sender ! flipState
  }

  val computationBehaviour: Receive = {
    case r @ Round(k) =>
      log.info(s"$cell > Initiating round $k by sending my state to $nbrs")
      nbrs.map(_ ! MyState(r, cell, state))
    case ms@MyState(r,_,s) =>
      gotOne(r,s)
  }

  def gotOne(r: Round, s: State) = {
    if(!rounds.contains(r)){
      nbrs.map(_ ! MyState(r, cell, state))
      rounds += r -> RoundData(0,0)
    }

    rounds += r -> rounds(r).add(s==Alive)

    if(rounds(r).got==nbrs.size){
      state = nextState(rounds(r).alives)
      context.parent ! MyState(r, cell, state)
    }
  }

  def nextState(alives: Int): State =
    if(state==Alive && (alives<2 || alives>3)) Dead
    else if(state==Dead && alives!=3) Dead
    else Alive

  def flipState = {
    state = if (state == Dead) Alive else Dead
    state
  }

  private case class RoundData(got: Int, alives: Int){
    def add(isAlive: Boolean) = RoundData(got+1, if(isAlive) alives+1 else alives)
  }
}
