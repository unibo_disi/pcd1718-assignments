package pcd1718.ass2.ex1;

import pcd1718.ass2.model.DataPiece;
import pcd1718.ass2.model.TaskData;
import pcd1718.ass2.model.TaskResult;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;
import java.util.function.Consumer;
import java.util.function.Function;

public class ForkJoinFolderSearcher extends RecursiveTask<TaskResult> {
    private TaskData data;
    private Consumer<TaskResult>  continuation;
    private Consumer<DataPiece> resultUpdate;

    public ForkJoinFolderSearcher(TaskData data, Consumer<TaskResult> continuation, Consumer<DataPiece> resultUpdate){
        this.data = data;
        this.continuation = continuation;
        this.resultUpdate = resultUpdate;
    }

    @Override
    protected TaskResult compute() {
        try {
            TaskResult result = new TaskResult();

            File[] files = new File(data.getBasePath()).listFiles();

            List<RecursiveTask<TaskResult>> dirTasks = new LinkedList<>();
            List<RecursiveTask<DataPiece>> fileTasks = new LinkedList<>();
            for(File f : files){
                if(f.isFile()) {
                    RecursiveTask<DataPiece> ftask = new ForkJoinFileSearcher(f, data.getRegex(), resultUpdate);
                    ftask.fork();
                    fileTasks.add(ftask);
                }
                else if(f.isDirectory() && data.getDepth()-1>0) {
                    RecursiveTask<TaskResult> dtask = new ForkJoinFolderSearcher(new TaskData(f.getPath(),data.getDepth()-1,data.getRegex()), null, resultUpdate);
                    dtask.fork();
                    dirTasks.add(dtask);
                }
            }

            for(RecursiveTask<DataPiece> ftask : fileTasks){
                DataPiece dp = ftask.join();
                result.put(dp.getPath(), dp.getCount());
            }

            for(RecursiveTask<TaskResult> dtask: dirTasks){
                TaskResult dr = dtask.join();
                result.merge(dr);
            }

            if(continuation!=null) continuation.accept(result);

            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
