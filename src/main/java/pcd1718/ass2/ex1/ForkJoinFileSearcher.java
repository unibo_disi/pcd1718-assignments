package pcd1718.ass2.ex1;
import pcd1718.ass2.utils.Utils;
import pcd1718.ass2.model.DataPiece;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.RecursiveTask;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public class ForkJoinFileSearcher extends RecursiveTask<DataPiece> {
    private File file;
    private Pattern pattern;
    private Consumer<DataPiece> resultUpdate;

    public ForkJoinFileSearcher(File file, Pattern pattern, Consumer<DataPiece> resultUpdate){
        this.file = file;
        this.pattern = pattern;
        this.resultUpdate = resultUpdate;
    }

    @Override
    protected DataPiece compute() {
        try {
            //long sizeInBytes = Files.size(file.toPath()); // TODO: handle big files
            String content = new String(Files.readAllBytes(file.toPath()));
            DataPiece result = new DataPiece(file.getAbsolutePath(), Utils.numMatches(pattern, content));
            resultUpdate.accept(result);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return new DataPiece(file.getAbsolutePath(), 0);
        }
    }
}
