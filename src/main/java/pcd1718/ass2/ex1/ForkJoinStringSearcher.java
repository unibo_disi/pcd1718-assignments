package pcd1718.ass2.ex1;
import pcd1718.ass2.utils.Utils;

import java.util.concurrent.RecursiveTask;
import java.util.regex.Pattern;

public class ForkJoinStringSearcher extends RecursiveTask<Integer> {
    private Pattern pattern;
    private String text;

    public ForkJoinStringSearcher(String text, Pattern pattern){
        this.text = text;
        this.pattern = pattern;
    }

    @Override
    protected Integer compute() {
        return Utils.numMatches(pattern, text);
    }
}
