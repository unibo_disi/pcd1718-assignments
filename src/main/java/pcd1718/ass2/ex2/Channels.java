package pcd1718.ass2.ex2;

public class Channels {
    public static String CHANNEL_DIRECTORY = "directory"; // directories to be processed
    public static String CHANNEL_FILE = "file"; // files to be processed
    public static String CHANNEL_END = "end"; // channel for end notifications
    public static String CHANNEL_EVENT = "event"; // application event
}
