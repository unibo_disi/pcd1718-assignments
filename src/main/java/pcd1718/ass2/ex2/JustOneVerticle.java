package pcd1718.ass2.ex2;

import io.reactivex.processors.FlowableProcessor;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.file.FileSystem;
import pcd1718.ass2.model.DataPiece;
import pcd1718.ass2.model.TaskData;
import pcd1718.ass2.utils.Utils;

public class JustOneVerticle extends AbstractVerticle {
    private TaskData data;
    private FlowableProcessor<DataPiece> emitter;

    public JustOneVerticle(TaskData data, FlowableProcessor<DataPiece> emitter){
        this.data = data;
        this.emitter = emitter;
    }

    @Override public void start(){
        traverse(data.getBasePath(), data.getDepth());
    }

    // Example of callback of hell :)
    public FileSystem traverse(String basePath, int depth){
        return vertx.fileSystem().readDir(basePath, files -> {
            VertxUtils.HandleError(files);
            if(files.succeeded()){
                files.result().forEach(file -> {
                    vertx.fileSystem().props(file, props -> {
                        VertxUtils.HandleError(props);
                        if(props.result().isDirectory() && depth-1>0) traverse(file, depth-1);
                        if(props.result().isRegularFile()) {
                            vertx.fileSystem().readFile(file, buffer -> {
                                VertxUtils.HandleError(buffer);
                                vertx.executeBlocking(future -> {
                                    future.complete(Utils.numMatches(data.getRegex(), buffer.toString()));
                                }, false, res -> {
                                    VertxUtils.HandleError(res);
                                    int numOccs = (int)res.result();
                                    emitter.onNext(new DataPiece(file, numOccs));
                                });
                            });
                        }
                    });
                });
            } else {
                System.err.println("Cannot read directory " + basePath);
            }
        });
    }
}
