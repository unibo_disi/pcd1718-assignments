package pcd1718.ass2.ex2;

import io.reactivex.processors.FlowableProcessor;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.file.FileSystem;
import pcd1718.ass2.model.DataPiece;
import pcd1718.ass2.model.TaskData;
import pcd1718.ass2.model.TaskDataFull;
import pcd1718.ass2.utils.Utils;

import java.io.File;

public class JustFileProcessingVerticle extends AbstractVerticle {
    private TaskDataFull data;
    private FlowableProcessor<DataPiece> emitter;

    public JustFileProcessingVerticle(TaskDataFull data, FlowableProcessor<DataPiece> emitter){
        this.data = data;
        this.emitter = emitter;
    }

    @Override public void start(){
        for(File file : data.getFiles()) {
            vertx.fileSystem().readFile(file.getAbsolutePath(), buffer -> {
                VertxUtils.HandleError(buffer);
                vertx.executeBlocking(future -> {
                    future.complete(Utils.numMatches(data.getRegex(), buffer.toString()));
                }, true, res -> {
                    VertxUtils.HandleError(res);
                    int numOccs = (int) res.result();
                    emitter.onNext(new DataPiece(file.getPath(), numOccs));
                });
            });
        }
    }
}
