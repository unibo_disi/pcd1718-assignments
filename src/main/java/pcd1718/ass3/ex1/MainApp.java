package pcd1718.ass3.ex1;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainApp extends Application {

    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ass3ex1.fxml"));
        Parent root = (Parent) loader.load();

        Scene scene = new Scene(root);

        stage.setOnCloseRequest(we -> { Platform.exit(); System.exit(0); });

        stage.setTitle("Assignment 3 - exercise 1");
        stage.setScene(scene);
        stage.show();
    }
}
