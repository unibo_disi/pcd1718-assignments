package pcd1718.ass3.ex1;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import pcd1718.ass3.oneToMany.*;

import java.net.URL;
import java.util.ResourceBundle;

public class ControllerApp implements Initializable {
    @FXML private TextField golRows;
    @FXML private TextField golCols;
    @FXML private TextField cellsPerActor;
    @FXML private Button setup;
    @FXML private Button startAndStop;
    @FXML private Button step;
    //@FXML private Slider zoom;
    @FXML private Canvas canvas;

    String startStr = "Start", stopStr = "Stop", pauseStr = "Pause", resumeStr = "Resume";

    ActorSystem actorSys;
    ActorRef golActor;
    ActorRef canvasActor;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        actorSys = ActorSystem.create("ass3ex1", ConfigFactory.defaultApplication()
                .withValue("akka.loglevel", ConfigValueFactory.fromAnyRef("DEBUG"))); // OFF, DEBUG, INFO
        canvasActor = actorSys.actorOf(Props.create(CanvasActor.class, canvas), "canvas");
        golActor = actorSys.actorOf(Props.create(GOLActor.class, canvasActor), "gol");

        canvas.setOnMouseClicked(evt -> {
            canvasActor.tell(Click.apply(evt.getX(), evt.getY()), ActorRef.noSender());
        });

        setup.setOnAction(event -> {
            Setup setup = Setup.apply(
                    Integer.parseInt(golRows.getText()),
                    Integer.parseInt(golCols.getText()),
                    Integer.parseInt(cellsPerActor.getText()));
            golActor.tell(setup, ActorRef.noSender());
            startAndStop.setDisable(false);
            step.setDisable(false);
        });

        step.setOnAction(event -> {
            golActor.tell(Tick$.MODULE$, ActorRef.noSender());
        });

        startAndStop.setOnAction(event -> {
            if(startAndStop.getText().equals(stopStr)) { // Stop
                golActor.tell(Stop$.MODULE$, ActorRef.noSender());
            } else { // Start
                golActor.tell(Start$.MODULE$, ActorRef.noSender());
            }
            startAndStop.setText(startAndStop.getText().equals(startStr) ? stopStr : startStr);
        });
    }

}
